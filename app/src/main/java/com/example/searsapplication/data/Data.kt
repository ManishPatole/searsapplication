package com.example.searsapplication.data

data class Data(val id: Int, val author: String, val title: String, val content: String)