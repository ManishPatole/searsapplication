@file:Suppress("DEPRECATION")

package com.example.searsapplication.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.View

/**
 * Checks internet connectivity
 */
fun isConnectedToInternet(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    return activeNetwork?.isConnectedOrConnecting == true
}

/**
 * Shows the view
 */
fun showViews(vararg views: View) {
    for (view in views) {
        view.visibility = View.VISIBLE
    }
}

/**
 * Hides the view
 */
fun hideViews(vararg views: View) {
    for (view in views) {
        view.visibility = View.GONE
    }
}