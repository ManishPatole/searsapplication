package com.example.searsapplication.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.searsapplication.R
import com.example.searsapplication.application.SearsApplication
import com.example.searsapplication.data.Data
import com.example.searsapplication.network.DetailStatus
import com.example.searsapplication.network.Details
import com.example.searsapplication.network.ExecutorCallback
import com.example.searsapplication.network.OnServerCallResponse
import com.example.searsapplication.util.isConnectedToInternet
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * View-model which processes user request.
 */
class MainViewModel(application: Application) : AndroidViewModel(application),
    OnServerCallResponse<List<Data?>?> {

    private var call: Call<List<Data?>?>? = null
    var detailStatus = MutableLiveData<DetailStatus>()
    private val context = getApplication<Application>().applicationContext

    @Inject
    lateinit var retrofit: Retrofit

    /**
     * Fetches all the comments.
     */
    fun getDetails() {
        if (isConnectedToInternet(context)) {
            (context.applicationContext as SearsApplication).retrofitComponent.inject(this)
            val service = retrofit.create(Details::class.java)
            call = service.listRepos()
            call?.enqueue(ExecutorCallback(this))
        } else {
            detailStatus.value = DetailStatus.Failure(context.getString(R.string.no_internet))
        }
    }

    override fun onSuccess(response: List<Data?>?) {
        detailStatus.value = DetailStatus.Success(response)
    }

    override fun onFailure() {
        detailStatus.value = DetailStatus.Failure(context.getString(R.string.something_went_wrong))
    }

    override fun onCleared() {
        super.onCleared()
        //Cancels the on-going call, as operation is not relevant to user anymore
        call?.apply {
            if (isExecuted) {
                cancel()
            }
        }
    }
}