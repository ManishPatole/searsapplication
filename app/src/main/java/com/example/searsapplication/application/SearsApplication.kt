package com.example.searsapplication.application

import android.app.Application
import com.example.searsapplication.network.DaggerRetrofitComponent
import com.example.searsapplication.network.RetrofitComponent

/**
 * Application class mainly used to create Retrofit component.
 */
class SearsApplication : Application() {

    lateinit var retrofitComponent: RetrofitComponent

    override fun onCreate() {
        super.onCreate()
        retrofitComponent = DaggerRetrofitComponent.create()
    }
}