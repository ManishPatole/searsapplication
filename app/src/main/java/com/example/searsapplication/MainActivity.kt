package com.example.searsapplication

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.searsapplication.adapter.DetailAdapter
import com.example.searsapplication.base.BaseActivity
import com.example.searsapplication.data.Data
import com.example.searsapplication.network.DetailStatus
import com.example.searsapplication.util.SwipeToDeleteCallback
import com.example.searsapplication.util.hideViews
import com.example.searsapplication.util.showViews
import com.example.searsapplication.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity<MainViewModel>() {

    private lateinit var adapter: DetailAdapter
    override lateinit var model: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialiseList()
        fetchDetails()
        handleSwipeToRefresh()
    }

    /**
     * Initiates the call and observes the details.
     */
    private fun fetchDetails() {
        model.getDetails()
        model.detailStatus.observe(this, Observer { status ->
            observeStatus(status)
            swipeRefresh.isRefreshing = false
        })
    }

    /**
     * Update the views according to status.
     */
    private fun observeStatus(status: DetailStatus) {
        when (status) {
            is DetailStatus.Failure -> {
                errorText.text = status.error
                hideViews(list, progressView)
                showViews(errorText)
            }
            is DetailStatus.Success -> displayList(status.detailList)
        }
    }

    /**
     * List initialisation and adding swipe to delete functionality.
     */
    private fun initialiseList() {
        adapter = DetailAdapter(null)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                (list.adapter as DetailAdapter).removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(list)
    }

    /**
     * Displays the list.
     */
    private fun displayList(dataList: List<Data?>?) {
        showViews(list)
        hideViews(progressView, errorText)
        adapter.updateList(dataList)
    }

    /**
     * Handles swipe to refresh.
     */
    private fun handleSwipeToRefresh() {
        swipeRefresh.setOnRefreshListener {
            model.getDetails()
        }
    }

    /**
     * Initialises the view model.
     */
    override fun initModel() = ViewModelProvider(this).get(MainViewModel::class.java)

}