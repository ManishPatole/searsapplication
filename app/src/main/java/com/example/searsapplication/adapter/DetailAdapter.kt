package com.example.searsapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.searsapplication.R
import com.example.searsapplication.data.Data
import com.example.searsapplication.util.hideViews
import com.example.searsapplication.util.showViews
import kotlinx.android.synthetic.main.row_detail.view.*

/**
 * Adapter to display list of comments.
 *
 * @author Manish Patole
 */
class DetailAdapter(private var detailList: List<Data?>?) :
    RecyclerView.Adapter<DetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_detail,
                parent, false
            )
        )
    }

    override fun getItemCount() = detailList?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val detail = detailList?.get(position)
        holder.author.text = detail?.author
        holder.title.text = detail?.title
        holder.description.text = detail?.content

        //Handles the visibility of read more button
        holder.description.post {
            if (holder.description.lineCount > 3) {
                showViews(holder.readMore)
            } else {
                hideViews(holder.readMore)
            }
        }

        holder.readMore.setOnClickListener {
            if (holder.readMore.text.toString() == holder.title.context.getString(R.string.read_more)) {
                holder.description.maxLines = Integer.MAX_VALUE
                holder.readMore.text = holder.title.context.getString(R.string.read_less)
            } else {
                holder.description.maxLines = 2
                holder.readMore.text = holder.title.context.getString(R.string.read_more)
            }
        }
    }

    fun updateList(detailList: List<Data?>?) {
        this.detailList = detailList
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        detailList?.drop(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val author: TextView = view.author
        val title: TextView = view.title
        val description: TextView = view.description
        val readMore: TextView = view.readMore
    }
}