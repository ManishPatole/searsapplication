package com.example.searsapplication.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Base activity which make sure view-model to respective activity is initialised before we use.
 */
abstract class BaseActivity<T> : AppCompatActivity() {
    abstract var model: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = initModel()
    }

    protected abstract fun initModel(): T
}