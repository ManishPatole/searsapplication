package com.example.searsapplication.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection

/**
 * Central place to get all the responses at first place to verify response.
 */
class ExecutorCallback<T>(private val onServerCallResponse: OnServerCallResponse<T>?) :
    Callback<T> {

    override fun onResponse(
        call: Call<T>, response: Response<T>
    ) {
        if (response.isSuccessful && response.code() == HttpURLConnection.HTTP_OK) {
            onServerCallResponse?.onSuccess(response.body())
        } else {
            onServerCallResponse?.onFailure()
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        onServerCallResponse?.onFailure()
    }

}