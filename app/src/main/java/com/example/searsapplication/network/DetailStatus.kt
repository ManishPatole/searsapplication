package com.example.searsapplication.network

import com.example.searsapplication.data.Data

/**
 * Status of API response
 */
sealed class DetailStatus {
    /**
     * Returns the data
     */
    class Success(val detailList: List<Data?>?) : DetailStatus()

    /**
     * Returns the error message
     */
    class Failure(val error: String) : DetailStatus()
}
