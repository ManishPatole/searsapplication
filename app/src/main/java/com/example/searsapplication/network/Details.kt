package com.example.searsapplication.network

import com.example.searsapplication.data.Data
import retrofit2.Call
import retrofit2.http.GET

interface Details {
    @GET("5e332eeb3200008abe94d46d")
    fun listRepos(): Call<List<Data?>?>?
}