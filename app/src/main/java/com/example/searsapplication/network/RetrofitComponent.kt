package com.example.searsapplication.network

import com.example.searsapplication.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class])
interface RetrofitComponent {
    fun inject(model: MainViewModel)
}