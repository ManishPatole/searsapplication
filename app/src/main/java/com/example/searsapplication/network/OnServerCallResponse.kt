package com.example.searsapplication.network

/**
 * Callback to update API response.
 */
interface OnServerCallResponse<T> {
    /**
     * Returns the response.
     */
    fun onSuccess(response: T?)

    /**
     * Indicates failure.
     */
    fun onFailure()
}