package com.example.searsapplication.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class RetrofitModule {

    @BaseUrl
    @Provides
    fun providesBaseUrl() = "http://www.mocky.io/v2/"

    @Provides
    fun providesGsonBuilder() = GsonBuilder()

    @Provides
    fun providesGson(gsonBuilder: GsonBuilder) = gsonBuilder.create()

    @Singleton
    @Provides
    fun provideRetrofit(
        @BaseUrl baseUrl: String,
        gson: Gson
    ) = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}